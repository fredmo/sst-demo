from flask import Flask, render_template, request, jsonify

from file_explorer import FileExplorer
from google_stt import GoogleSST

app = Flask(__name__)

@app.route('/')
def homepage():
    file_ex = FileExplorer()
    print(file_ex.browse())

    return render_template("index.html", files=file_ex.browse())

@app.route('/data')
def data():
    path = request.args.get('path')
    sstrecognizer = GoogleSST()
    res = {
        'transcripts': sstrecognizer.recognize_by_duration_chunk(path=path)
    }
    return jsonify(res)
    # pass

if __name__ == '__main__':
    app.run(host='0.0.0.0', use_reloader = True, debug = True)