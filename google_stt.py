from google.cloud import speech
from google.cloud.speech import enums
from google.cloud.speech import types
import os
import io
import json
import datetime

# audio handling
from pydub import AudioSegment
from pydub.utils import make_chunks
# from vad.vad import Vad

os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = './voicebot-248106-80d7ae6f85cc.json'

class GoogleSST:
    def __init__(self):
        self.client = speech.SpeechClient()
        self.config = types.RecognitionConfig(
            encoding=enums.RecognitionConfig.AudioEncoding.LINEAR16,
            sample_rate_hertz=8000,
            language_code='zh',
            audio_channel_count=1,
            # model='phone_call',
            # use_enhanced=True,
            # speech_contexts=[{
            #     "phrases": [
            #         "ELN",
            #         "ELI",
            #         "ELN Linked",
            #         "ELI Linked",
            #         "CCBA",
            #         "Non CCBA",
            #         "HSBC",
            #         "RMB",
            #         "ETF",
            #         "建設投資銀行",
            #         "建設銀行",
            #         "全數撇帳",
            #         "term sheet",
            #         "offering circular",
            #         "產品細則",
            #         "HSBC Bank PLC",
            #         "Product Name",
            #         "Issuer",
            #         "HKT",
            #         "Serial number",
            #         "Currency",
            #         "Product Tenor",
            #         "Product Term",
            #         "Initial Fixing Date",
            #         "Issue Date",
            #         "Final Fixing Date",
            #         "Maturity Date",
            #         "Issue Price",
            #         "Strike Price",
            #         "Fix Coupon",
            #         "per annum",
            #         "call feature",
            #         "call price",
            #         "login feature",
            #         "AKI",
            #         "login price",
            #         "Monitorial Benefit",
            #         "無",
            #         "係",
            #         "product risk rating level",
            #         "收市價",
            #         "行使價",
            #         "贖回價",
            #         "實物結算",
            #         "匯豐",
            #         "發行商",
            #         "唔使",
            #         "保本",
            #         "潛在現金收益",
            #         "潛在資本增值",
            #         "潛在非現金收益",
            #         "本行",
            #         "Memorandum",
            #         "Product Booklet",
            #         "Financial Disclosure Document",
            #         "發行人",
            #         "hold fund"
            #     ]
            # }]

            # enable_separate_recognition_per_channel=True,
        )


    def _check_result_exist(self, path):
        return os.path.isdir('./static/json/{}'.format(path.split('/')[-1]))

    def _create_json_result(self, path, data):
        with open('./static/json/{}.json'.format(path.split('/')[-1]), 'w') as fout:
            json.dump(data, fout)

    def recognize_by_duration_chunk(self, path, duration=59000):
        transcript_start = datetime.datetime.now().replace(microsecond=0)
        result = []

        _as = AudioSegment.from_wav('.{}'.format(path))

        chunks = make_chunks(_as, duration)

        for idx, chunk in enumerate(chunks):
            _path = './static/temp/{}_{}.wav'.format(path.split('/')[-1], idx+1)
            create_wav_start = datetime.datetime.now().replace(microsecond=0)
            self._create_temp_wav_from_as(chunk, _path)
            create_wav_end = datetime.datetime.now().replace(microsecond=0)

            print('create_wav_file {} took {}.'.format(_path, create_wav_end - create_wav_start))


            call_start = datetime.datetime.now().replace(microsecond=0)
            result = result + self._speech_to_text(_path)
            call_end = datetime.datetime.now().replace(microsecond=0)
            print('google api call {} took {}.'.format(_path, call_end - call_start))

        self._create_json_result(path, result)

        transcript_end = datetime.datetime.now().replace(microsecond=0)
        print('Transcripting {} took {}.'.format(path, transcript_end-transcript_start))

        return result

    def _create_temp_wav_from_as(self, segment, path):
        segment.export(path, format='wav')

    def _speech_to_text(self, path):
        _result = ''
        _result_array = []

        with io.open(path, 'rb') as audio_file:
            content = audio_file.read()
        
        audio = types.RecognitionAudio(
            content=content
        )

        responses = self.client.recognize(self.config, audio)

        # print('responses', responses)

        for result in responses.results:
            # print('result', result)
            # print('alternatives', result.alternatives)
            _result= result.alternatives[0].transcript if _result == '' else _result + ' {}'.format(result.alternatives[0].transcript)
            _result_array.append({
                'transcript': result.alternatives[0].transcript,
                'confidence:': result.alternatives[0].confidence
            })

        # self._create_json_result(path, _result_array)
        
        return _result_array

    def recognize_by_speaker_changes(self, path = './static/audio/20190827_1638_83759_83774.Elena.wav'):
        pass
        # _result = ''
        # print('testing path', path)
        # _as = AudioSegment.from_wav('.{}'.format(path))
        #
        # vad = Vad(path)
        # for idx, s, e in vad.detect_speaker_changes():
        #     _path = './static/temp/{}_{}.wav'.format(path.split('/')[-1], idx+1)
        #
        #     self._create_temp_wav_from_as(_as[s:e], _path)


            # result = self._speech_to_text(_path)
            
            # print('t', idx, s, e, result)
        #     try:
        #         for response in responses:
        #             print('response', response)
        #             for result in response.results:
        #                 print('result', result)
        #                 alternatives = result.alternatives
        #                 print('alternatives', alternatives)
        #                 for alternative in alternatives:
        #                     print('alternative', alternative)
        #                     result= results.alternatives.transcript if result == '' else result + ' {}'.format(alternative.transcript)
        #     except:
        #         pass
        
        # print(result)
        return _result

g = GoogleSST ()

g.recognize_by_duration_chunk('/static/audio/Mandarin1.wav')
g.recognize_by_duration_chunk('/static/audio/Mandarin2.wav')
